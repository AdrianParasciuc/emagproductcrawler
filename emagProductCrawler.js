var request = require('request'),
    cheerio = require('cheerio'),
    categoriesUrls = [],
    productsUrls = [],
    paginationNumberRegex = /\/p\d+\/c/;

var emagBaseUrl = 'http://www.emag.ro';
request(emagBaseUrl, function(err, response, html) {
    if (!err && response.statusCode == 200) {
        var $ = cheerio.load(html);
        $('#emg-mega-menu div.emg-menu-box div.emg-megamenu-column a.emg-megamenu-link').each(function(element) {
            var url = $(this).attr('href');
            categoriesUrls.push(emagBaseUrl + url);
        });

        // categoriesUrls.forEach(function(categoryUrl) {
        // 	request(categoryUrl, function(err, response, html) {
        // 		var $ = cheerio.load(html);
        // 		$('div.product-holder-grid div.poza-produs a.link_imagine').each(function(element) {
        // 			var url = $(this).attr('href');
        // 			productsUrls.push(emagBaseUrl + url);
        // 		});
        // 	});	
        // });

        request(categoriesUrls[100], function(err, response, html) {
            if (!err && response.statusCode == 200) {
                var $ = cheerio.load(html);
                $('div.product-holder-grid div.poza-produs a.link_imagine').each(function(element) {
                    var url = $(this).attr('href');
                    productsUrls.push(emagBaseUrl + url);
                });
                var pages = $('#emg-pagination-numbers a.emg-pagination-no');
                var pageLink = $(pages[1]).attr('href');
                var splittedPageLink = $(pages[pages.length - 1]).attr('href').split('/');
                var lastPageNumber = splittedPageLink[splittedPageLink.length - 2].slice(1);

                for (var i = 2; i <= lastPageNumber; i++) {
                    var pageNumber = '/p' + i + '/c';
                    var pageUrl = emagBaseUrl + pageLink.replace(paginationNumberRegex, pageNumber);
                    console.log(pageUrl);
                }

                console.log(categoriesUrls[100]);
            }
        });
    }
});
